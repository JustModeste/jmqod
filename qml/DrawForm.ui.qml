import QtQuick
import QtQuick.Layouts

Item {
    id: item1
    width: 480
    height: 70
    property alias img: img

    RowLayout {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            text: qsTr("Draw")
        }
        JMImgButton {
            id: img
            enabled: false
            color: "#22ff22"
            height: 50
            width: 50
            source: "qrc:///images/00.png"
        }
    }
}
