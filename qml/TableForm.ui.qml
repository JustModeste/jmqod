import QtQuick 2.4

Item {
    id: iRoot
    width: 480
    height: 400

    property int minSize: Math.min((iRoot.width - 40) / 3,(iRoot.height - 50) /4)
    property int numberSelected: 0

    property alias b01: b01
    property alias b02: b02
    property alias b03: b03
    property alias b04: b04
    property alias b05: b05
    property alias b06: b06
    property alias b07: b07
    property alias b08: b08
    property alias b09: b09
    property alias b10: b10
    property alias bResult: bResult

    Column {
        id: column
        anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10

            JMButton {
                id: bResult
                width: 200
                height: 100
                border.width: 1
                radius: 20
                visible: false
            }

            Row {
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter
                JMImgButton {
                    id: b01
                    source: "qrc:///images/01.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b02
                    source: "qrc:///images/02.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b03
                    source: "qrc:///images/03.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
            }

            Row {
                visible: true
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter
                JMImgButton {
                    id: b04
                    source: "qrc:///images/04.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b05
                    source: "qrc:///images/05.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b06
                    source: "qrc:///images/06.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
            }

            Row {
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter
                JMImgButton {
                    id: b07
                    source: "qrc:///images/07.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b08
                    source: "qrc:///images/08.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
                JMImgButton {
                    id: b09
                    source: "qrc:///images/09.png"
                    width: minSize
                    height: minSize
                    visible: false
                }
            }

            Row {
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter
                JMImgButton {
                    id: b10
                    source: "qrc:///images/10.png"
                    width: minSize
                    height: minSize
                    visible: false
                }

            }


    }
    states: [
        State {
            name: "Step1"

            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }
        },
        State {
            name: "Step2"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }
        },
        State {
            name: "Step3"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }
        },
        State {
            name: "Step4"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
                state: ""
            }
        },
        State {
            name: "Step5"
            PropertyChanges {
                target: b01
                visible: true
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
            }

            PropertyChanges {
                target: b06
                visible: true
                state: ""
            }
        },
        State {
            name: "Step6"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b06
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b07
                visible: true
                state: ""
            }
        },
        State {
            name: "Step7"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
            }

            PropertyChanges {
                target: b06
                visible: true
            }

            PropertyChanges {
                target: b07
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b08
                visible: true
                state: ""
            }
        },
        State {
            name: "Step8"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b06
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b07
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b08
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b09
                visible: true
                state: ""
            }
        },
        State {
            name: "Step9"
            PropertyChanges {
                target: b01
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b02
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b03
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b04
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b05
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b06
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b07
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b08
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b09
                visible: true
                state: ""
            }

            PropertyChanges {
                target: b10
                visible: true
                state: ""
            }
        },
        State {
            name: "Win"

            PropertyChanges {
                target: bResult
                visible: true
                text: "You WIN"
                topColor: "#0cf528"
                bottomColor: "#c9f8d3"
            }
        },
        State {
            name: "Lose"

            PropertyChanges {
                target: bResult
                visible: true
                text: "You LOSE"
                textColor: "#ffffff"
                topColor: "#da0632"
                bottomColor: "#f09faa"
            }

            PropertyChanges {
                target: iRoot
                visible: true
            }
        }
    ]



}
