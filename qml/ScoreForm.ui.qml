import QtQuick

Rectangle {
    id: rRoot
    width: 480
    height: 70
    color: "#b923a2e7"

    property alias credits: credits
    property alias gains: gains

    Row {
        id: row
        anchors.fill: parent

        Text {
            text: qsTr("Credits")
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            width: rRoot.width / 10 * 2
            color: "#ffffff"
        }

        JMItem {
            id: credits
            anchors.verticalCenter: parent.verticalCenter
            width:  (rRoot.width / 10 * 3) - 5
            text: "9"
        }

        Text {
            text: qsTr("Gains")
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            width: rRoot.width / 10 * 2
            color: "#ffffff"
        }

        JMItem {
            id: gains
            anchors.verticalCenter: parent.verticalCenter
            width:  (rRoot.width / 10 * 3) - 5
            text: "1"
        }
    }
}
