import QtQuick 2.4

TableForm {
    signal selected(bool value)

    function changeStates(button) {
        if (button.state === "click") {
            if (button !== b01) {
                b01.state = "";
            }
            if (button !== b02) {
                b02.state = "";
            }
            if (button !== b03) {
                b03.state = "";
            }
            if (button !== b04) {
                b04.state = "";
            }
            if (button !== b05) {
                b05.state = "";
            }
            if (button !== b06) {
                b06.state = "";
            }
            if (button !== b07) {
                b07.state = "";
            }
            if (button !== b08) {
                b08.state = "";
            }
            if (button !== b09) {
                b09.state = "";
            }
            if (button !== b10) {
                b10.state = "";
            }
        }
        if (b01.state === "click" || b02.state === "click" || b03.state === "click" || b04.state === "click" || b05.state === "click"
                || b06.state === "click" || b07.state === "click" || b08.state === "click" || b09.state === "click" || b10.state === "click") {
            selected(true)
        } else {
            selected(false);
            numberSelected = 0;
        }
    }

    b01.onStateChanged: {
        changeStates(b01);
        numberSelected = 1;
    }
    b02.onStateChanged: {
        changeStates(b02);
        numberSelected = 2;
    }
    b03.onStateChanged: {
        changeStates(b03);
        numberSelected = 3;
    }
    b04.onStateChanged: {
        changeStates(b04);
        numberSelected = 4;
    }
    b05.onStateChanged: {
        changeStates(b05);
        numberSelected = 5;
    }
    b06.onStateChanged: {
        changeStates(b06);
        numberSelected = 6;
    }
    b07.onStateChanged: {
        changeStates(b07);
        numberSelected = 7;
    }
    b08.onStateChanged: {
        changeStates(b08);
        numberSelected = 8;
    }
    b09.onStateChanged: {
        changeStates(b09);
        numberSelected = 9;
    }
    b10.onStateChanged: {
        changeStates(b10);
        numberSelected = 10;
    }
}
