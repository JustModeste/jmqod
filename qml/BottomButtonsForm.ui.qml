import QtQuick 2.4

Item {
    id: iRoot
    width: 400
    height: 70

    property alias bQuit: bQuit
    property alias bDouble: bDouble

    Row {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10

        JMButton {
            id: bQuit
            text: qsTr("Quit")
            width: (iRoot.width - 30) / 2
        }

        JMButton {
            id: bDouble
            text: qsTr("Double")
            width: (iRoot.width - 30) / 2
            visible: false
        }
    }
}
