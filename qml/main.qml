import QtQuick


Window {
    id: wRoot
    width: 480
    height: 854
    visible: true
    title: qsTr("JM Quit or Double")

    property alias buttons: buttons
    property alias gains: score.gains


    Column {
        id: column1

        Score {
            id: score
            width: wRoot.width
            height: 70
         }

        Draw {
            id: draw
            width: wRoot.width
            height: 70

        }

        Table {
            id: table
            width: wRoot.width
            height: wRoot.height - score.height - draw.height - buttons.height
            state: "Step1"

            onSelected: {
                buttons.bDouble.visible = value
            }

            bResult.onClicked: {
                canPlayAgain();
            }
        }



        BottomButtons {
            id: buttons
            width: wRoot.width
            height: 70

            bDouble.onClicked: {
                displayDraw();
            }
        }
    }
    buttons.onQuitted: {
        score.credits.text = parseInt(score.credits.text) + parseInt(score.gains.text);
        score.gains.text = 0;
        let credits = parseInt(score.credits.text);
        if (credits > 0) {
            score.credits.text = credits - 1;
            table.state = "Step1";
            qod.step = 1;
        } else {
            table.state = "Lose";
            close()
        }
    }

    function canPlayAgain() {
        let credits = parseInt(score.credits.text);
        if (credits > 0) {
            score.credits.text = credits - 1;
            table.state = "Step1";
            qod.step = 1;
        } else {
            table.state = "Lose";
        }
    }

    function displayDraw() {
        let step = parseInt(table.state.replace("Step", ""))
        qod.step = step;
        let numberDraw = qod.draw;
        var image = "qrc:///images/";
        if (numberDraw < 10) {
            image += "0";
        }
        image += numberDraw;
        image += ".png";

        draw.img.source = image;

        if (qod.check(table.numberSelected)) {
            changeStep();
        } else {
            table.state = "Lose";
            score.gains.text = "0"
        }
    }

    function changeStep() {
        let newState = parseInt(table.state.replace("Step", "")) + 1;
        qod.step = newState;
        gains.text = qod.gains();
        if (newState > 9) {
            table.state = "Win";
        } else {
            table.state = "Step" + newState;
        }
    }
}
