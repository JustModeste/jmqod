import QtQuick 2.4

JMItem {
    property alias mouseArea: mouseArea
    property alias enabled: mouseArea.enabled
    property string colorSave: jmItem.topColor

    id: jmItem
    width: 100
    height: 40
    radius: 5

    MouseArea {
        id: mouseArea
        width: 100
        height: 50

        anchors.fill: parent
    }
}
