#ifndef QUITORDOUBLE_H
#define QUITORDOUBLE_H

#include <QObject>
#include <QRandomGenerator>
#include <QtDebug>

class QuitOrDouble : public QObject
{
    Q_OBJECT
private:
    int _step;
    int _random;
public:
    Q_PROPERTY(int step READ getStep WRITE setStep NOTIFY stepChanged);
    Q_PROPERTY(int draw READ draw NOTIFY drawChanged);

    explicit QuitOrDouble(QObject *parent = nullptr);

    int getStep(void);
    void setStep(int value);

    int draw(void);

    Q_INVOKABLE bool check(int userValue);
    Q_INVOKABLE int gains();
signals:
    void stepChanged(void);
    void drawChanged(void);
};

#endif // QUITORDOUBLE_H
