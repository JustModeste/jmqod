#include "quitordouble.h"

QuitOrDouble::QuitOrDouble(QObject *parent)
    : QObject{parent}
{

}

int QuitOrDouble::getStep(void) {
    return this->_step;
}

void QuitOrDouble::setStep(int value) {
    this->_step = value;
}


/**
 * @brief QuitOrDouble::draw    Effectue un tirage en fonction de l'étape
 * @param step  numéro de l'étape
 * @return le numbre aléatoire
 */
int QuitOrDouble::draw(void) {
    this->_random = 0;

    if (this->_step > 0 && this->_step < 9) {
        this->_random = QRandomGenerator::global()->bounded(this->_step + 1) + 1;
    }
    qDebug() << "Random = " << this->_random;
    return this->_random;
}

bool QuitOrDouble::check(int userValue) {
    return userValue == this->_random;
}

int QuitOrDouble::gains() {
    int ret = 0;
    qDebug() << "Step " << this->_step;

    switch (this->_step) {
        case 1:
            ret = 1;
        break;
        case 2:
            ret = 5;
        break;
        case 3:
            ret = 20;
        break;
        case 4:
            ret = 100;
        break;
        case 5:
            ret = 700;
        break;
        case 6:
            ret = 5000;
        break;
        case 7:
            ret = 40000;
        break;
        case 8:
            ret = 350000;
        break;
        case 9:
            ret = 3000000;
        break;
            default:
            ret = 0;
        break;

    }
    return ret;
}
